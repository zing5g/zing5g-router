#ifndef _Included_com_zing5g_router_NATIVE_ZING5G_H_
#define _Included_com_zing5g_router_NATIVE_ZING5G_H_

#include <jni.h>
#include <string>
#include <cstdint>

#define MAX_USB_ETHERNET_DEVICE     2
#define MAX_INTERFACE_NAME          4   // eth1, eth2

std::string execCommand(const char* );

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jstring JNICALL
Java_com_zing5g_router_Zing5GNetworking_stringFromJNI(JNIEnv* , jobject);

JNIEXPORT jint JNICALL
Java_com_zing5g_router_Zing5GNetworking_getListOfUsbLans(JNIEnv* , jobject, jobjectArray, jint);

// These typedefs and function prototypes can be moved to a separate header file
typedef struct {
    int32_t     mUsbPort;
    char        mInterface[MAX_INTERFACE_NAME + 1];
} UsbEthernetDevice;

typedef struct {
    UsbEthernetDevice mDevices[MAX_USB_ETHERNET_DEVICE];
    int32_t     mNumDevices;
} UsbEthernetList;

int addUsbEthernetDevice(UsbEthernetList* pList, int32_t port, char* iface);
void dumpUsbEthernetList();

#ifdef __cplusplus
}
#endif
#endif  // _Included_com_zing5g_router_NATIVE_ZING5G_H_