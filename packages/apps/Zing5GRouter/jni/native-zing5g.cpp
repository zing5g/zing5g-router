
#include <cstdio>
#include <array>
#include <stdexcept>
#include <iostream>
#include <memory>
#include <sstream>
#include "native-zing5g.h"

#include <android/log.h>

static UsbEthernetList usbEthernetList;

// Uncomment this to enable debugging messages
//#define ZING_JNI_DEBUG

// UsbEthernet Java class path and methods signatures.
// The methods signatures were obtained from
// javap -s out/soong/.intermediates/packages/apps/Zing5GRouter.../UsbEthernet.class
#define USB_ETHERNET_CLASS_PATH                 "com/zing5g/router/UsbEthernet"
#define USB_ETHERNET_SET_PORT_SIGNATURE         "(I)V"
#define USB_ETHERNET_SET_INTERFACE_SIGNATURE    "(Ljava/lang/String;)V"

// Command to grab USB Ethernet information from file system
#define USB_ETHERNET_IFACE_CMD  "ls -al /sys/class/net | grep usb | sed -e 's/.*usb2\\/2-1\\/2-1\\.//' | cut -d/ -f 1,4"

#define USB_ETHERNET_NAME_SIZE  5  // Examples: eth1, eth2, ...
#define USB_PORT_IFACE_SIZE     6  // Examples: 2/eth1, 3/eth2

JNIEXPORT jstring JNICALL
Java_com_zing5g_router_Zing5GNetworking_stringFromJNI(JNIEnv* env, jobject) {
    std::string hello = "Hello Zing5G from C++";
    return env->NewStringUTF(hello.c_str());
}


JNIEXPORT jint JNICALL
Java_com_zing5g_router_Zing5GNetworking_getListOfUsbLans(JNIEnv* env, jobject,
            jobjectArray pUsbEthernetArray, jint length) {

    if (!length) {
         __android_log_print(ANDROID_LOG_ERROR, __FUNCTION__, "Array length is 0");
        return 0;
    }

    // The method signature was obtain from
    // javap -s out/soong/.intermediates/packages/apps/Zing5GRouter.../UsbEthernet.class
    jmethodID setPort;
    jmethodID setInterface;
    jclass jc = env->FindClass(USB_ETHERNET_CLASS_PATH);
    if (jc == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, __FUNCTION__, "UsbEthernet Java class was NOT found");
        return 0;
    }

    setPort = env->GetMethodID(jc, "setPort", USB_ETHERNET_SET_PORT_SIGNATURE);
    if (setPort == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, __FUNCTION__, "Couldn't get UsbEthernet setPort");
        return 0;
    }

    setInterface = env->GetMethodID(jc, "setInterface", USB_ETHERNET_SET_INTERFACE_SIGNATURE);
    if (setInterface == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, __FUNCTION__, "Couldn't get UsbEthernet setInterface");
        return 0;
    }

    std::string usbEthernet;
    usbEthernet = execCommand(USB_ETHERNET_IFACE_CMD);

    int i = 0;
    if (!usbEthernet.empty()) {
        usbEthernetList.mNumDevices = 0;
        std::stringstream ss(usbEthernet);
        std::string line;

        char iface[USB_ETHERNET_NAME_SIZE];
        int port;
        while(!ss.eof() && i < length) {
            std::getline(ss, line);
            // line format is port/interface. For example 2/eth1. Maximun size of line is 6.
            if (line.size() > USB_PORT_IFACE_SIZE) {
                __android_log_print(ANDROID_LOG_ERROR, __FUNCTION__, "Invalid string size:%s, %d", line.c_str(), (int)line.size());
                continue;
            }
            if (!line.empty()) {
                if (std::sscanf(line.c_str(), "%d/%s", &port, iface) != 2) {
                    // sscanf should read 2 arguments from the line.
                    __android_log_print(ANDROID_LOG_ERROR, __FUNCTION__, "Error reading port and iface %s", line.c_str());
                    continue;
                }
#ifdef ZING_JNI_DEBUG
                __android_log_print(ANDROID_LOG_INFO, __FUNCTION__, "%d- %s, port:%d, iface:%s", i + 1, line.c_str(), port, iface);
#endif
                if (addUsbEthernetDevice(&usbEthernetList, port, iface)) {
                    __android_log_print(ANDROID_LOG_ERROR, __FUNCTION__, "failed to add new USB Ethernet device");
                    break;
                }

                // Update jobjectArray entries
                // Create local object
                jobject localUE = env->GetObjectArrayElement(pUsbEthernetArray, i);
                if (localUE == NULL) {
                    __android_log_print(ANDROID_LOG_ERROR, __FUNCTION__, "NULL localUE");
                    break;
                }

                // Set USB port of local object
                env->CallVoidMethod(localUE, setPort, port);

                // Create jstring from iface char*, set interface name of local object
                jstring jInterface = env->NewStringUTF(iface);
                env->CallVoidMethod(localUE, setInterface, jInterface);

                // Copy back updated local object to the jobjectArray
                env->SetObjectArrayElement(pUsbEthernetArray, i, localUE);
                i++;
            }
        }
#ifdef ZING_JNI_DEBUG
        dumpUsbEthernetList();
#endif
    }

    return i;
}

std::string execCommand(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        __android_log_print(ANDROID_LOG_ERROR, __FUNCTION__, "failed to open pipe");
        return result;
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }

#ifdef ZING_JNI_DEBUG
    if (result.empty()) {
        __android_log_print(ANDROID_LOG_INFO, __FUNCTION__, "result empty");
    } else {
        __android_log_print(ANDROID_LOG_INFO, __FUNCTION__, "%s", result.c_str());
    }
#endif

    return result;
}

// Return:
//      - 0 success
//      - -1 failure
int addUsbEthernetDevice(UsbEthernetList* pList, int32_t port, char* iface) {
    if (!iface && !pList && pList->mNumDevices >= MAX_USB_ETHERNET_DEVICE) {
        return -1;
    }

    UsbEthernetDevice* pDevice = pList->mDevices + pList->mNumDevices++;
    pDevice->mUsbPort = port;
    strncpy(pDevice->mInterface, iface, MAX_INTERFACE_NAME);
    pDevice->mInterface[MAX_INTERFACE_NAME] = '\0';
    return 0;
}

void dumpUsbEthernetList() {
    int size = usbEthernetList.mNumDevices;
    __android_log_print(ANDROID_LOG_INFO, __FUNCTION__, "USB Ethernet list size:%d", size);
    if (size == 0) {
        return;
    }

    if (size > MAX_USB_ETHERNET_DEVICE) {
        __android_log_print(ANDROID_LOG_ERROR, __FUNCTION__, "Invalid list size");
        usbEthernetList.mNumDevices = 0;
        return;
    }

    for (int i = 0; i < size; ++i) {
        __android_log_print(ANDROID_LOG_INFO, __FUNCTION__, "%d- port:%d iface:%s", i + 1, 
                usbEthernetList.mDevices[i].mUsbPort, usbEthernetList.mDevices[i].mInterface);
    }
}

