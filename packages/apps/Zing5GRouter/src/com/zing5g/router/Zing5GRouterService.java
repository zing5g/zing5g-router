package com.zing5g.router;

import android.os.HandlerThread;
import android.os.Handler;
import android.os.Message;
import android.os.Looper;
import android.os.PowerManager;
import android.os.ServiceManager;
import android.util.Log;
import android.os.INetworkManagementService;
import android.os.RemoteException;
import android.net.InterfaceConfiguration;
import android.net.LinkAddress;
import android.net.RouteInfo;
import android.net.IpPrefix;
import java.net.InetAddress;
import java.net.Inet4Address;

import android.content.Context;
import android.provider.Settings;
import android.content.ContentResolver;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.net.NetworkRequest;
import android.net.NetworkCapabilities;
import static android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET;
import static android.net.NetworkCapabilities.TRANSPORT_WIFI;
import static android.net.NetworkCapabilities.TRANSPORT_CELLULAR;
import static android.net.NetworkCapabilities.TRANSPORT_ETHERNET;
import android.net.LinkProperties;
import android.net.dhcp.DhcpServerCallbacks;
import android.net.dhcp.DhcpServingParamsParcel;
import android.net.dhcp.DhcpServingParamsParcelExt;
import android.net.dhcp.IDhcpServer;
import static android.net.dhcp.IDhcpServer.STATUS_SUCCESS;
import android.net.NetworkStackClient;
import android.net.INetworkStackStatusCallback;
import android.os.RemoteException;
import android.os.ServiceSpecificException;

import static com.android.internal.util.Preconditions.checkNotNull;

import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Zing5GRouterService {
    private static final String TAG = Zing5GRouterService.class.getSimpleName();
    private static final boolean DBG = true;
    private static Zing5GRouterService mInstance;
    private Context mAppContext;
    private HandlerThread mHandlerThread;
    private Zing5GRouterHandler mHandler;
    private ConnectivityManager mCM;
    private Zing5GRouterNetworkCallback mNetworkCallback;
    private Zing5GNetworkObserver mNetworkObserver;
    private INetworkManagementService mNMS;
    public  UsbEthernet[] mUsbEthernetDevices;
    private IDhcpServer mDhcpServer;
    private DhcpServingParamsParcelExt mUsbLanDhcpServerParams;
    private Zing5GNetworkStackStatusCallback mUsbLanNetworkStatusCB;

    // These items could be configurable
    private static final String USB_LAN_IFACE_ADDR = "192.168.60.1";
    private static final String USB_LAN_IFACE_SUBNET = "192.168.60.0";
    private static final int USB_LAN_PREFIX_LENGTH = 24;
    private static final int USB_LAN_DHCP_LEASE_TIME_SECS = 3600;
    private static final int DHCP_SERVER_UPDATE_PARAMS_INTERVAL = 2000; // Milliseconds

    private static final int MAX_NET_ID = 65535; // From ConnectivityService.java. 0xFFFF
    private static final int USB_LAN_TABLE_DEST_USB_LAN_NET_ID = MAX_NET_ID + 1;
    private static final int USB_LAN_TABLE_DEST_WAN_NET_ID = MAX_NET_ID + 2;
    private static final int WAN_TABLE_DEST_USB_LAN_NET_ID = MAX_NET_ID + 3;
    private static final int LOCAL_NETWORK_TABLE_DEST_USB_LAN_NET_ID = MAX_NET_ID + 4;

    // Android ip rule priorities are > 10000
    private static final int USB_LAN_IP_RULE_PRIORITY = 200;

    // Currently we assume there is only ONE LAN port
    private String mLanIface;

    // Temporary mechanism to get the USB LAN port via JNI periodic query
    private int mTestCounter = 0;
    private static final int JNI_QUERIES = 10;
    private static final int JNI_QUERY_INTERVAL = 2000; // Milliseconds

    // Network ID is the key for the HashMap
    private HashMap<Integer, WanLinkPropertiesTransport> mWanLpT;

    public static final int ZING_5G_ROUTER_BOOTED = 1;
    public static final int WAN_INTERFACE_AVAILABLE = 2;
    public static final int WAN_INTERFACE_UP = 3;
    public static final int WAN_INTERFACE_DOWN = 4;
    public static final int USB_LAN_INTERFACE_ADDED = 5;
    public static final int GET_USB_ETHERNET_INTERFACES = 6;
    public static final int UPDATE_DHCP_SERVER_PARAMS = 7;
    public static final int MODIFY_DEFAULT_ROUTE_TO_USB_LAN = 8;

    public static final int ADD_DEFAULT_ROUTE = 1;
    public static final int REMOVE_DEFAULT_ROUTE = 0;

    // Type of WAN transport
    private static final int UNKNOWN_WAN_TYPE = -1;
    public static final int WIFI_WAN_TYPE  = 1;
    public static final int CELLULAR_WAN_TYPE = 2;
    public static final int ETHERNET_WAN_TYPE = 3;

    // WAN ports priorities for setting the USB LAN default route
    public static final int CELLULAR_WAN_PRIORTY = 3;
    public static final int ETHERNET_WAN_PRIORTY = 2;
    public static final int WIFI_WAN_PRIORTY = 1;

    private final Zing5GRouterStateMachine mRouterSM;
    
    private class Zing5GRouterHandler extends Handler {
        public Zing5GRouterHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ZING_5G_ROUTER_BOOTED:
                    handleRouterBooted();
                    handleUsbEthernetInterfaces();
                    break;

                case WAN_INTERFACE_AVAILABLE:
                    handleWanInterfaceAvailable((Network) msg.obj);
                    break;

                case WAN_INTERFACE_UP:
                    if (handleWanInterfaceUp(msg.arg1, msg.arg2, (LinkProperties) msg.obj)) {
                        handleUpdateRoutingTable();
                    }
                    break;

                case WAN_INTERFACE_DOWN:
                    handleWanInterfaceDown(msg.arg1);
                    handleUpdateRoutingTable();
                    break;

                case USB_LAN_INTERFACE_ADDED:
                    String iface = (String) msg.obj;
                    handleUsbLanIfaceAdded(iface);
                    break;

                case GET_USB_ETHERNET_INTERFACES:
                    handleUsbEthernetInterfaces();
                    break;

                case UPDATE_DHCP_SERVER_PARAMS:
                    handleUpdateDhcpServerParams();
                    break;

                case MODIFY_DEFAULT_ROUTE_TO_USB_LAN:
                    handleDefaultRoute(msg.arg1, msg.arg2);
                    break;

                default:
                    break;
            }
        }
    }

    private class Zing5GDhcpServerCallbacks extends DhcpServerCallbacks {
        @Override
        public void onDhcpServerCreated(int statusCode, IDhcpServer server) throws RemoteException {
            // This call back is called from NetworkStackService$NetworkStackConnector binder thread
            // Here we post a runnable in our thread to start the server. Since all Zing5G
            // routing services, such as DHCP server configuration / start are running in the same
            // thread, hence no need for any synchronization for class fields.
            mHandler.post(() -> {
                Log.i(TAG, "onDhcpServerCreated statusCode: " + statusCode);
                if (statusCode != STATUS_SUCCESS) {
                    Log.e(TAG, "Failed obtaining DHCP server: " + statusCode);
                    return;
                }

                mDhcpServer = server;

                // Start DHCP server
                Log.i(TAG, "Starting DHCP server");
                try {
                    mDhcpServer.start(mUsbLanNetworkStatusCB);
                } catch (RemoteException e) {
                    Log.e(TAG, "Exception starting DHCP server: " + e);
                    e.rethrowFromSystemServer();
                }
            });
        }
    }

    private class Zing5GNetworkStackStatusCallback extends INetworkStackStatusCallback.Stub {
        @Override
        public void onStatusAvailable(int startStatusCode) {
            // Run the processing in our thread
            mHandler.post(() -> {
                Log.i(TAG, "onStatusAvailable statusCode: " + startStatusCode);
                if (startStatusCode != STATUS_SUCCESS) {
                    Log.e(TAG, "Error starting DHCP server: " + startStatusCode);
                } else {
                    Log.i(TAG, "DHCP server started successfully, notify SM");

                    createLanIfaceLookup(mIface);

                    // Notify router state machine about LAN interface
                    mRouterSM.lanPortDetected(mIface);
                }
            });
        }

        @Override
        public int getInterfaceVersion() {
            return this.VERSION;
        }

        private String mIface;
        public void setIface(String iface) {
            mIface = iface;
        }
    }

    public static Zing5GRouterService getInstance(Context appContext) {
        if (mInstance == null) {
            try {
                mInstance = new Zing5GRouterService(appContext);
            } catch (Exception e) {
                throw new RuntimeException("Exception in Zing5GRouterService constructor");
            }
        }
        return mInstance;
    }

    private Zing5GRouterService(Context appContext) {
        mAppContext = checkNotNull(appContext, "Missing Context");
        mCM = mAppContext.getSystemService(ConnectivityManager.class);
        mLanIface = new String();
        mNetworkCallback = new Zing5GRouterNetworkCallback();
        mWanLpT = new HashMap<>();
        mUsbEthernetDevices = new UsbEthernet[UsbEthernet.USB_ETHERNET_PORTS];
        for (int i = 0; i < UsbEthernet.USB_ETHERNET_PORTS; ++i) {
            mUsbEthernetDevices[i] = new UsbEthernet();
            // Need to call set methods so the methods are loaded in VM.
            // Later on the JNI code tries to call GetMethodID on them.
            mUsbEthernetDevices[i].setPort(0);
            mUsbEthernetDevices[i].setInterface("");
        }

        mUsbLanDhcpServerParams = new DhcpServingParamsParcelExt();
        mDhcpServer = null;
        mUsbLanNetworkStatusCB = new Zing5GNetworkStackStatusCallback();

        mHandlerThread = new HandlerThread("Zing5GRouterService");
        mHandlerThread.start();

        mHandler = new Zing5GRouterHandler(mHandlerThread.getLooper());

        mRouterSM = new Zing5GRouterStateMachine(this,
                CELLULAR_WAN_PRIORTY, ETHERNET_WAN_PRIORTY, WIFI_WAN_PRIORTY);
        if (DBG) {
            Log.d(TAG, "Starting Zing5GRouterStateMachine");
        }

        mRouterSM.start();
    }

    public void sendMessage(int what) {
        Message msg = mHandler.obtainMessage(what);

        if (!mHandler.sendMessage(msg)) {
            Log.e(TAG, "Failed to send message to handler");
        }
    }

    public void sendMessage(int what, Object obj) {
        Message msg = mHandler.obtainMessage(what, obj);

        if (!mHandler.sendMessage(msg)) {
            Log.e(TAG, "Failed to send message to handler with obj");
        }
    }

    public void sendMessage(int what, int arg1, Object obj) {
        Message msg = mHandler.obtainMessage(what, arg1, 0, obj);

        if (!mHandler.sendMessage(msg)) {
            Log.e(TAG, "Failed to send message to handler with arg1 and obj");
        }
    }

    public void sendMessage(int what, int arg1, int arg2, Object obj) {
        Message msg = mHandler.obtainMessage(what, arg1, arg2, obj);

        if (!mHandler.sendMessage(msg)) {
            Log.e(TAG, "Failed to send message to handler with arg1, arg2, and obj");
        }
    }

    public void sendMessageDelayed(int what, long delayMillis) {
        Message msg = mHandler.obtainMessage(what);

        if (!mHandler.sendMessageDelayed(msg, delayMillis)) {
            Log.e(TAG, "Failed to send message with delayed");
        }
    }

    private void handleRouterBooted() {
        if (DBG)
            Log.d(TAG, "handleRouterBooted");

        ContentResolver cr = mAppContext.getContentResolver();

        if (DBG) {
            int currentValue = Settings.Global.getInt(cr, Settings.Global.WIFI_ALWAYS_REQUESTED, -1);
            Log.i(TAG, "Current WIFI_ALWAYS_REQUESTED: " + currentValue);

            currentValue = Settings.Global.getInt(cr, Settings.Global.SOFT_AP_TIMEOUT_ENABLED, -1);
            Log.i(TAG, "Current SOFT_AP_TIMEOUT_ENABLED: " + currentValue);
        }

        if (!Settings.Global.putInt(cr, Settings.Global.WIFI_ALWAYS_REQUESTED, 1)) {
            Log.e(TAG, "Error in setting Settings.Global.WIFI_ALWAYS_REQUESTED");
        } else {
            if (DBG)
                Log.i(TAG, "Set WIFI_ALWAYS_REQUESTED to 1");
        }

        if (!Settings.Global.putInt(cr, Settings.Global.SOFT_AP_TIMEOUT_ENABLED, 0)) {
            Log.e(TAG, "Error in setting Settings.Global.SOFT_AP_TIMEOUT_ENABLED to 0");
        } else {
            if (DBG)
                Log.i(TAG, "Set SOFT_AP_TIMEOUT_ENABLED to 0");
        }


        if (DBG) {
            Log.i(TAG, "Registerring network callback");
        }
        registerNetworkCallback();

        mNMS = INetworkManagementService.Stub.asInterface(
                ServiceManager.getService(Context.NETWORKMANAGEMENT_SERVICE));
        mNetworkObserver = new Zing5GNetworkObserver(this);

        registerNetworkObserver();

        // Get list of interfaces
        String[] ifaces = null;
        try {
            ifaces = mNMS.listInterfaces();
        } catch (Exception e) {
            Log.e(TAG, "Error listing interfaces", e);
        }

        Log.i(TAG, "Number of interfaces: " + ifaces.length);
        int i = 1;
        for (String iface : ifaces) {
            Log.i(TAG, i + "- " + iface);
            i++;
        }
    }

    private void handleWanInterfaceAvailable(Network network) {
        int netId = network.netId;
        if (DBG)
            Log.d(TAG, "handleWanInterfaceAvailable netId: " + netId);
        if (mWanLpT.containsKey(netId)) {
            if (DBG) {
                Log.d(TAG, "netId: " + netId + " is already in the map");
            }
            mWanLpT.replace(netId, null);
        } else {
            mWanLpT.put(netId, null);
        }
    }

    /*
     * Return: - true: update routing table - false: don't update routing table
     */
    private boolean handleWanInterfaceUp(int netId, int wanType, LinkProperties linkProperties) {
        if (DBG) {
            Log.d(TAG, "handleWanInterfaceUp netId: " + netId + ", wanType: " + wanType);
            Log.i(TAG, "LinkProperties: " + linkProperties.toString());
        }

        if (wanType != WIFI_WAN_TYPE &&
                wanType != CELLULAR_WAN_TYPE &&
                wanType != ETHERNET_WAN_TYPE) {
            Log.e(TAG, "handleWanInterfaceUp netId: " + netId
                    + ", invalid WAN type: " + wanType);
            return false;
        }

        IpPrefix wanIPv4Subnet = null;
        InetAddress wanGateway = null;
        Object[] prefixAndGateway = getWanIPv4SubnetAndDefaultGateway(linkProperties);
        if (prefixAndGateway[0] == null) {
            Log.e(TAG, "WAN subnet is null");
            return false;
        } else {
            wanIPv4Subnet = (IpPrefix) prefixAndGateway[0];
        }
        if (prefixAndGateway[1] == null) {
            Log.e(TAG, "WAN gateway is null");
            return false;
        } else {
            wanGateway = (InetAddress) prefixAndGateway[1];
        }

        if (mWanLpT.get(netId) != null) {
            Log.w(TAG, "handleWanInterfaceUp netId: " + netId + ", is already in the map");
            return false;
        }

        String wanIface = linkProperties.getInterfaceName();
        if (DBG) {
            Log.d(TAG, "handleWanInterfaceUp WAN " + wanIface
                    + ", subnet: " + wanIPv4Subnet.toString()
                    + ", gateway: " +  wanGateway.getHostAddress()
            );
        }

        mWanLpT.put(netId, new WanLinkPropertiesTransport(linkProperties, wanType));

        // Update USB LAN routing table with WAN subnet and gateway.
        addToUsbLanTableWanRoute(wanIface, wanIPv4Subnet);
        addToWanTableUsbLanRoute(wanIface);
        // For multiple WAN interfaces the State Machine decides which WAN
        // subnet and gateway should be used for USB LAN routing table.
        mRouterSM.wanInterfaceUp(wanType, wanIface, netId);

        return true;
    }

    private void handleWanInterfaceDown(int netId) {
        if (DBG)
            Log.d(TAG, "handleWanInterfaceDown netId: " + netId);

        if (mWanLpT.containsKey(netId)) {
            int wanType = mWanLpT.get(netId).transport;
            LinkProperties linkProperties = mWanLpT.get(netId).lp;
            if (linkProperties != null) {
                String wanIface = linkProperties.getInterfaceName();
                if (DBG) {
                    Log.d(TAG, "handleWanInterfaceDown iface: " + wanIface
                            + ", WAN type: " + wanType);
                }
                mRouterSM.wanInterfaceDown(wanType, wanIface);
            } else {
                Log.e(TAG, "handleWanInterfaceDown netId: " + netId + " null link properties");
            }
            mWanLpT.remove(netId);
        } else {
            Log.e(TAG, "handleWanInterfaceDown netId:" + netId + " does not exist");
        }
    }

    private void handleUpdateRoutingTable() {
        if (DBG)
            Log.d(TAG, "handleUpdateRoutingTable");
        // TODO 
    }

    private void handleUsbLanIfaceAdded(String iface) {
        if (DBG)
            Log.d(TAG, "handleUsbLanIfaceAdded iface: " + iface);
        
        if (mLanIface.isEmpty()) {
            mLanIface = iface;
            Log.i(TAG, "USB LAN interface added: " + iface);
            // Start DHCP server on the LAN
        } else {
            Log.e(TAG, "New USB LAN: " + iface + ", current: " + mLanIface);
        }
    }

    private void handleUsbEthernetInterfaces() {
        if (DBG)
            Log.d(TAG, "handleUsbEthernetInterfaces " + mTestCounter);
        if (mLanIface.isEmpty() && (++mTestCounter < JNI_QUERIES)) {
            int n = Zing5GNetworking.getListOfUsbLans(mUsbEthernetDevices, mUsbEthernetDevices.length);
            Log.d(TAG, "JNI call USB Ethernet interface(s): " + n);
            for (int i = 0; i < n; ++i) {
                if (DBG)
                    Log.d(TAG, (i + 1) + "- port: " + mUsbEthernetDevices[i].getPort() +
                            ", iface: " + mUsbEthernetDevices[i].getInterface());
                if (mUsbEthernetDevices[i].isLanInterface()) {
                    mLanIface = mUsbEthernetDevices[i].getInterface();
                    mTestCounter = 0;
                    if (DBG)
                        Log.i(TAG, "LAN interface is: " + mLanIface);

                    // Configure LAN interface
                    if (!configureLanIface(mLanIface))
                        break;

                    // Start DHCP server on it
                    if (!startDhcpServer(mLanIface))
                        break;

                    // Notification to router state machine about LAN interface is sent
                    // after DHCP server is started in Zing5GNetworkStackStatusCallback

                    return;
                }
            }
            sendMessageDelayed(GET_USB_ETHERNET_INTERFACES, JNI_QUERY_INTERVAL);
        } else {
            Log.d(TAG, "handleUsbEthernetInterfaces mLanIface empty or test counter: " + mTestCounter);
        }
    }

    private void handleDefaultRoute(int netId, int add) {
        if (DBG) {
            Log.d(TAG, "handleDefaultRoute netId: " + netId + ", add: " + add);
        }

        if (!mWanLpT.containsKey(netId)) {
            Log.e(TAG, "handleDefaultRoute netId: " + netId + " is not in the map");
            return;
        }

        LinkProperties lp = mWanLpT.get(netId).lp;
        InetAddress defaultGateway = getDefaultGateway(lp);
        if (defaultGateway == null) {
            Log.e(TAG, "handleDefaultRoute netId: " + netId + " does not have default gateway");
            return;
        }

        if (add == ADD_DEFAULT_ROUTE) {
            addToUsbLanTableWanDefaultGateway(lp.getInterfaceName(), defaultGateway);
        } else {
            removeFromUsbLanTableWanDefaultGateway(lp.getInterfaceName(), defaultGateway);
        }
    }

    private InetAddress getDefaultGateway(LinkProperties lp) {
        InetAddress retGateway = null;
        List<RouteInfo> linkRoutes = lp.getRoutes();
        for (RouteInfo route : linkRoutes) {
            if (route.hasGateway()) {
                retGateway = route.getGateway();
                Log.d(TAG, "WAN " + route.getInterface()
                        + " defatult gateway is " + retGateway.toString());
                break;
            }
        }
        return retGateway;
    }

    private Object[] getWanIPv4SubnetAndDefaultGateway(LinkProperties wanLinkProperties) {
        IpPrefix retDestination = null;
        InetAddress retGateway = null;
        List<RouteInfo> linkRoutes = wanLinkProperties.getRoutes();
        for (RouteInfo route : linkRoutes) {
            Log.d(TAG, "route: " + route.toString()
                    + ", hasGateway: " + route.hasGateway()
            );
            IpPrefix destination = route.getDestination();
            Log.d(TAG, "Destination: " + destination.toString()
                    + ", isIPv4: " + destination.isIPv4());

            if (!route.hasGateway() && destination.isIPv4()) {
                Log.d(TAG, "WAN " + route.getInterface()
                        + " subnet to be added to USB LAN: "
                        + destination.toString());
                retDestination = destination;
                continue;
            }

            if (route.hasGateway()) {
                retGateway = route.getGateway();
                Log.d(TAG, "WAN " + route.getInterface()
                        + " gateway is " + retGateway.toString());
            }
        }

        return new Object[]{retDestination, retGateway};
    }

    private boolean startDhcpServer(String iface) {
        try {
            InetAddress ifaceAddress = InetAddress.getByName(USB_LAN_IFACE_ADDR);
                                        // Throws UnknownHostException

            LinkAddress linkAddress = new LinkAddress(ifaceAddress, USB_LAN_PREFIX_LENGTH);
            mUsbLanDhcpServerParams.setDefaultRouters((Inet4Address) ifaceAddress);
            mUsbLanDhcpServerParams.setDhcpLeaseTimeSecs(USB_LAN_DHCP_LEASE_TIME_SECS);
            mUsbLanDhcpServerParams.setDnsServers((Inet4Address) ifaceAddress);
            mUsbLanDhcpServerParams.setServerAddr(linkAddress);
            mUsbLanDhcpServerParams.setMetered(false);

            // Create DhcpServerCallbacks object
            Zing5GDhcpServerCallbacks cb = new Zing5GDhcpServerCallbacks();
            // The interface on which DHCP server starts is passed to mUsbLanNetworkStatusCB
            // since it will be used as part of LAN detected event sent to router state machine.
            mUsbLanNetworkStatusCB.setIface(iface);

            // Call for a DhcpServer object
            Log.i(TAG, "Calling NetworkStackClient makeDhcpServer");
            NetworkStackClient.getInstance().makeDhcpServer(iface, mUsbLanDhcpServerParams, cb);
        } catch (Exception e) {
            Log.e(TAG, "Exception starting DHCP server on " + iface + ", " + e);
            return false;
        }
        return true;
    }

    private boolean configureLanIface(String iface) {
        try {
            InterfaceConfiguration ifaceConfig = mNMS.getInterfaceConfig(iface);
            if (ifaceConfig == null) {
                Log.e(TAG, "configureLanIface couldn't get InterfaceConfiguration for " + iface);
                return false;
            }
            InetAddress ifaceAddress = InetAddress.getByName(USB_LAN_IFACE_ADDR);
                                        // Throws UnknownHostException
            LinkAddress linkAddress = new LinkAddress(ifaceAddress, USB_LAN_PREFIX_LENGTH);
            Log.i(TAG, "Configuring " + iface + " with IP address " + USB_LAN_IFACE_ADDR);
            ifaceConfig.setLinkAddress(linkAddress);
            ifaceConfig.setInterfaceUp();
            mNMS.setInterfaceConfig(iface, ifaceConfig);
        } catch (Exception e) {
            Log.e(TAG, "Exception in configuring interface: " + iface + ", " + e);
            return false;
        }
        return true;
    }

    private void createLanIfaceLookup(String iface) {
        String prefix = USB_LAN_IFACE_SUBNET + "/" + USB_LAN_PREFIX_LENGTH;
        RouteInfo route = new RouteInfo(new IpPrefix(prefix), null, iface);

        Log.d(TAG, "createLanIfaceLookup route:" + route.toString() + ", route type:" + route.getType());
        try {
            mNMS.addRoute(USB_LAN_TABLE_DEST_USB_LAN_NET_ID, route);
            mNMS.addRoute(LOCAL_NETWORK_TABLE_DEST_USB_LAN_NET_ID, route);
        } catch (RemoteException | ServiceSpecificException e) {
            Log.e(TAG, "createLanIfaceLookup got exception: " + e);
        }
    }

    // Add route to USB LAN subnet in WAN interface table
    private void addToWanTableUsbLanRoute(String wanIface) {
        if (mLanIface.isEmpty()) {
            Log.e(TAG, "addToWanTableUsbLanRoute USB LAN interface is empty.");
            return;
        }

        addLanWanRoute(WAN_TABLE_DEST_USB_LAN_NET_ID, wanIface,
                new IpPrefix(USB_LAN_IFACE_SUBNET + "/" + USB_LAN_PREFIX_LENGTH), null);
    }

    // Add route to WAN subnet in USB LAN interface table
    private void addToUsbLanTableWanRoute(String wanIface, IpPrefix subnet) {
        if (mLanIface.isEmpty()) {
            Log.e(TAG, "addToUsbLanTableWanRoute USB LAN interface is empty.");
            return;
        }

        addLanWanRoute(USB_LAN_TABLE_DEST_WAN_NET_ID, wanIface, subnet, null);
    }

    private void addToUsbLanTableWanDefaultGateway(String wanIface, InetAddress gateway) {
        if (mLanIface.isEmpty()) {
            Log.e(TAG, "addToUsbLanTableWanDefaultGateway USB LAN interface is empty.");
            return;
        }

        if (wanIface.isEmpty()) {
            Log.e(TAG, "addToUsbLanTableWanDefaultGateway WAN iface is empty");
            return;
        }

        addLanWanRoute(USB_LAN_TABLE_DEST_WAN_NET_ID, wanIface, null, gateway);
    }

    private void addLanWanRoute(int netId, String wanIface, IpPrefix subnet, InetAddress gateway) {
        try {
            RouteInfo route = new RouteInfo(subnet, gateway, wanIface);

            Log.d(TAG, "addLanWanRoute netId:" + netId
                    + ", route:" + route.toString()
                    + ", route type:" + route.getType());

            mNMS.addRoute(netId, route);
        } catch (RemoteException | ServiceSpecificException e) {
            Log.e(TAG, "addLanWanRoute got exception: " + e);
        }
    }

    private void removeFromUsbLanTableWanDefaultGateway(String wanIface, InetAddress gateway) {
        if (mLanIface.isEmpty()) {
            Log.e(TAG, "removeFromUsbLanTableWanDefaultGateway USB LAN interface is empty.");
            return;
        }

        if (wanIface.isEmpty()) {
            Log.e(TAG, "removeFromUsbLanTableWanDefaultGateway WAN iface is empty");
            return;
        }

        removeLanWanRoute(USB_LAN_TABLE_DEST_WAN_NET_ID, wanIface, null, gateway);
    }

    private void removeLanWanRoute(int netId, String wanIface, IpPrefix subnet, InetAddress gateway) {
        try {
            RouteInfo route = new RouteInfo(subnet, gateway, wanIface);

            Log.d(TAG, "removeLanWanRoute netId:" + netId
                    + ", route:" + route.toString()
                    + ", route type:" + route.getType());

            mNMS.removeRoute(netId, route);
        } catch (RemoteException | ServiceSpecificException e) {
            Log.e(TAG, "removeLanWanRoute got exception: " + e);
        }
    }


    private boolean updateDhcpServerParams() {
        boolean updated = false;
        if (mDhcpServer != null) {
            Log.d(TAG, "updateDhcpServerParams DHCP server has already started.");
            try {
                mDhcpServer.updateParams(mUsbLanDhcpServerParams, mUsbLanNetworkStatusCB);
                updated = true;
            } catch(RemoteException e) {
                Log.e(TAG, "Could not update DHCP server parameters " + e);
            }
        } else {
            Log.d(TAG, "DHCP server has NOT started");
        }
        return updated;
    }

    private void handleUpdateDhcpServerParams() {
        if (DBG) {
            Log.d(TAG, "handleUpdateDhcpServerParams");
        }

        if (updateDhcpServerParams() == false) {
            sendMessageDelayed(UPDATE_DHCP_SERVER_PARAMS, DHCP_SERVER_UPDATE_PARAMS_INTERVAL);
        }
    }

    private void registerNetworkCallback() {
        // Register call back for WiFi, Cellular, and Ethernet networks
        int[] transports = { TRANSPORT_WIFI, TRANSPORT_CELLULAR, TRANSPORT_ETHERNET };
        for (int transport : transports) {
            NetworkRequest request = new NetworkRequest.Builder().addCapability(NET_CAPABILITY_INTERNET)
                    .addTransportType(transport).build();

            mCM.registerNetworkCallback(request, mNetworkCallback);
        }
    }

    private void registerNetworkObserver() {
        Log.i(TAG, "registerNetworkObserver");
        try {
            mNMS.registerObserver(mNetworkObserver);
        } catch (RemoteException e) {
            Log.e(TAG, "Could not register Zing5GNetworkObserver " + e);
        }
    }

    private void networkInterfaces() {
        Enumeration<NetworkInterface> interfacesList;
        try {
            interfacesList = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException ex) {
            Log.e(TAG, ex.getMessage());
            return;
        }

        for (NetworkInterface e : Collections.list(interfacesList)) {
            Log.i(TAG, e.toString());
        }
    }

    private int getWanType(Network network) {
        int wanType = UNKNOWN_WAN_TYPE;

        NetworkCapabilities nc = mCM.getNetworkCapabilities(network);
        if (nc.hasTransport(TRANSPORT_WIFI)) {
            Log.i(TAG, "getWanType for WiFi WAN");
            wanType = WIFI_WAN_TYPE;
        } else {
            if (nc.hasTransport(TRANSPORT_CELLULAR)) {
                Log.i(TAG, "getWanType for Cellular WAN");
                wanType = CELLULAR_WAN_TYPE;
            } else {
                if (nc.hasTransport(TRANSPORT_ETHERNET)) {
                    Log.i(TAG, "getWanType for Ethernet WAN");
                    wanType = ETHERNET_WAN_TYPE;
                }
            }
        }
        return wanType;
    }

    private final class Zing5GRouterNetworkCallback extends ConnectivityManager.NetworkCallback {
        @Override
        public void onLinkPropertiesChanged(Network network, LinkProperties linkProperties) {
            // Posting the processing to be executed in Zing5GRouter thread, instead of in
            // ConnectivityManager's.
            mHandler.post(() -> {
                if (linkProperties.hasIpv4Address()) {
                    int wanType = getWanType(network);
                    if (wanType != UNKNOWN_WAN_TYPE) {
                        sendMessage(WAN_INTERFACE_UP, network.netId, wanType, linkProperties);
                    } else {
                        Log.e(TAG, "onLinkPropertiesChanged UNKNOWN_WAN_TYPE for netId: "
                                + network.netId);
                    }
                } else {
                    Log.i(TAG, "onLinkPropertiesChanged no IPv4 address");
                }
            });
        }

        @Override
        public void onAvailable(Network network) {
            mHandler.post(() -> {
                if (DBG) {
                    Log.i(TAG, "NetworkCallback onAvailable: " + network.toString());
                }

                sendMessage(WAN_INTERFACE_AVAILABLE, network);
            });
        }

        @Override
        public void onLost(Network network) {
            mHandler.post(() -> {
                int netId = network.netId;
                if (DBG) {
                    Log.i(TAG, "NetworkCallback onLost: " + network.toString());
                }

                sendMessage(WAN_INTERFACE_DOWN, netId, null);
            });
        }
    }

    private final class WanLinkPropertiesTransport {
        public LinkProperties lp;
        public int transport;

        WanLinkPropertiesTransport(LinkProperties lp, int transport) {
            this.lp = lp;
            this.transport = transport;
        }
    }

}
