package com.zing5g.router;

import android.util.Log;
import com.android.server.net.BaseNetworkObserver;

public class Zing5GNetworkObserver extends BaseNetworkObserver{
    private static final String TAG = Zing5GNetworkObserver.class.getSimpleName();
    private Zing5GRouterService mZing5GRouterService;

    public Zing5GNetworkObserver(Zing5GRouterService zrs) {
        mZing5GRouterService = zrs;
    }

    @Override
    public void interfaceStatusChanged(String iface, boolean up) {
        Log.i(TAG, "interfaceStatusChanged: " + iface);
    }

    @Override
    public void interfaceRemoved(String iface) {
        Log.i(TAG, "interfaceRemoved " + iface);
    }

    @Override
    public void interfaceAdded(String iface) {
        Log.i(TAG, "interfaceAdded " + iface);
    }

    @Override
    public void usbLanInterfaceAdded(String iface) {
        Log.i(TAG, "usbLanInterfaceAdded " + iface);
        mZing5GRouterService.sendMessage(Zing5GRouterService.USB_LAN_INTERFACE_ADDED, iface);
    }
}
