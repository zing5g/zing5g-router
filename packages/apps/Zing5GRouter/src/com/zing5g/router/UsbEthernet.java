package com.zing5g.router;

public class UsbEthernet {
    public static final int USB_ETHERNET_PORTS = 2;  // There are 2 USB Ethernet ports on platform
    public static final int USB_ETHERNET_LAN_PORT = 3;  // USB Ethernet device connected to port 3 USB hub is LAN interface
    public static final int USB_ETHERNET_WAN_PORT = 2;  // USB Ethernet device connected to port 3 USB hub is WAN interface
    private int mUsbPort;
    private String mInterface;

    public UsbEthernet() {
        mUsbPort = 0;
        mInterface = "";
    }

    public UsbEthernet(int usbPort, String iface) {
        mUsbPort = usbPort;
        mInterface = iface;
    }

    public boolean isLanInterface() {
        return mUsbPort == USB_ETHERNET_LAN_PORT;
    }

    public String getInterface() {
        return mInterface;
    }

    public void setInterface(String iface) {
        mInterface = iface;
    }

    public int getPort() {
        return mUsbPort;
    }
    public void setPort(int port) {
        mUsbPort = port;
    }
}
