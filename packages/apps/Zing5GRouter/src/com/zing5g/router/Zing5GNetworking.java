package com.zing5g.router;

import android.util.Log;

public class Zing5GNetworking {
    // Load native library on application startup
    static {
        System.loadLibrary("native-zing5g");
    }

    static public native String stringFromJNI();
    static public native int getListOfUsbLans(UsbEthernet[] pUsbEthernetArray, int length);

}