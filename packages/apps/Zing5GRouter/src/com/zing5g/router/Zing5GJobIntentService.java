package com.zing5g.router;


import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

public class Zing5GJobIntentService extends JobIntentService {
    private static final String TAG = Zing5GJobIntentService.class.getSimpleName();
    private static final boolean DBG = true;
    protected static WifiManager mWifiManager;
    protected WifiConfiguration mApConfiguration;

    public static final String GET_AP_CONFIG = "get_ap_config";
    public static final int ZING_5G_JOB_ID = 22;

    public static void enqueueWork(Context context, Intent intent) {
        mWifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        enqueueWork(context, Zing5GJobIntentService.class, ZING_5G_JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (DBG) Log.i(TAG, "onHandleWork");
        boolean apConfig = intent.getBooleanExtra("AP config", false);
        
        if (apConfig) {
            getApConfiguration();
        }
    }

    private void getApConfiguration() {
        if (DBG) Log.i(TAG, "Received get AP config");
        mApConfiguration = mWifiManager.getWifiApConfiguration();
        Log.i(TAG, "getApConfiguration " + mApConfiguration.toString());
        Log.i(TAG, " Some default AP configurations:");
        String configTag = "    - ";
        Log.i(configTag, mApConfiguration.SSID);
        Log.i(configTag, mApConfiguration.preSharedKey);
    }
}
