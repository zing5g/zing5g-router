package com.zing5g.router;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;


public class MainActivity extends AppCompatActivity {
    static final boolean DEBUG = true;
    private static final String TAG = MainActivity.class.getSimpleName();
    private final String LOGFILE = "/data/data/com.zing5g.router/Z5GR.log";
    private final boolean EXTERNAL_LOGFILE = false;
    private TextView mTextView;
    private Button mStartButton;    // The button is added for connecting debugger to the running app

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextView = findViewById(R.id.textView);

        // Keep display on for testing USB-Ethernet adapter
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mStartButton = findViewById(R.id.button);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                if (DEBUG) {
                    Calendar calendar = Calendar.getInstance();
                    String currentTime = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());
                    StringBuilder stringBuilder = new StringBuilder("Started: " + currentTime);
                    Log.i(TAG, "5G Router configuration APP started" );
                    writeLog(stringBuilder.toString());
                    readLog();
                }

                Intent jobIntent = new Intent(MainActivity.this, Zing5GJobIntentService.class);
                jobIntent.putExtra("AP config", false);
                Zing5GJobIntentService.enqueueWork(MainActivity.this, jobIntent);
            }
        });

        Log.i(TAG, Zing5GNetworking.stringFromJNI());
        TextView jniTextView = findViewById(R.id.textViewJni);
        jniTextView.setText(Zing5GNetworking.stringFromJNI());
    }

    public boolean isExternalStorageWritable() {

        if (!EXTERNAL_LOGFILE || Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            // TODO: The permission for the extranl storage access should be checked here or in the caller.
            return true;
        }
        return false;
    }

    public boolean isExternalStorageReadable() {
        if (!EXTERNAL_LOGFILE ||
                Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(Environment.getExternalStorageState())
        ) {
            // TODO: The permission for the extranl storage access should be checked here or in the caller.
            return true;
        }
        return false;
    }

    public void writeLog(String message) {
        if (isExternalStorageWritable()) {
            try {
                File logFile = new File(LOGFILE);
                FileOutputStream writeLog = new FileOutputStream(logFile);
                writeLog.write(message.getBytes());
                writeLog.close();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, "Error writing log file", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void readLog() {
        if (isExternalStorageReadable()) {
            StringBuilder stringBuilder = new StringBuilder();
            try {
                FileReader fileReader = new FileReader(LOGFILE);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String newLine = null;
                while ((newLine = bufferedReader.readLine()) != null) {
                    stringBuilder.append(newLine + "\n");
                }

                mTextView.setText(stringBuilder);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, "Error reading log file", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Cannot read exteranl storage", Toast.LENGTH_LONG).show();
        }
    }
}