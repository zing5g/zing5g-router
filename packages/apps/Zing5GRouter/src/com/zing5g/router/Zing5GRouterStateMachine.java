package com.zing5g.router;

import com.android.internal.util.State;
import com.android.internal.util.StateMachine;

import android.os.Message;

import java.io.StringReader;
import java.io.BufferedReader;
import java.util.HashMap;

public class Zing5GRouterStateMachine extends StateMachine{
    private static final String TAG = Zing5GRouterStateMachine.class.getSimpleName();

    private static final int CMD_LAN_DETECTED = 1;
    private static final int CMD_WIFI_WAN_UP = 2;
    private static final int CMD_WIFI_WAN_DOWN = 3;
    private static final int CMD_CELLULAR_WAN_UP = 4;
    private static final int CMD_CELLULAR_WAN_DOWN = 5;
    private static final int CMD_ETHERNET_WAN_UP = 6;
    private static final int CMD_ETHERNET_WAN_DOWN = 7;

    private static final String[] msgWhat = {
      "Invalid message",
      "CMD_LAN_DETECTED",
      "CMD_WIFI_WAN_UP",
      "CMD_WIFI_WAN_DOWN",
      "CMD_CELLULAR_WAN_UP",
      "CMD_CELLULAR_WAN_DOWN",
      "CMD_ETHERNET_WAN_UP",
      "CMD_ETHERNET_WAN_DOWN",
    };

    private String mLanIface = new String();
    private String mWifiWan = new String();
    private String mCellularWan = new String();
    private String mEthernetWan = new String();
    private Zing5GRouterService mZRS;
    // To get WAN interface netId from its name
    private HashMap<String, Integer> mIfaceNetId;

    private int mCellularPriority;
    private int mEthernetPriority;
    private int mWifiPriority;

    private String getMessageName(int what) {
        if (what >= msgWhat.length || what < 0) {
            return "Unknown message";
        }
        return msgWhat[what];
    }

    public Zing5GRouterStateMachine(Zing5GRouterService zrs,
                                    int cP, int eP, int wP) {
        super(TAG);
        log("ctor E cellular:" + cP + ", Ethernet:" + eP + ", wifi:" + wP);

        mCellularPriority = cP;
        mEthernetPriority = eP;
        mWifiPriority = wP;
        mZRS = zrs;
        mIfaceNetId = new HashMap<>();

        addState(mWaitForLan);
        addState(mLanUp);
        addState(mWanC);
        addState(mWanW);
        addState(mWanE);
        addState(mWanCW);
        addState(mWanWE);
        addState(mWanCE);
        addState(mWanWCE);

        setInitialState(mWaitForLan);

        log("ctor X");
    }

    public void lanPortDetected(String iface) {
        sendMessage(CMD_LAN_DETECTED, iface);
    }

    public void wanInterfaceUp(int transport, String iface, int netId) {
        int what;
        switch(transport) {
            case Zing5GRouterService.WIFI_WAN_TYPE:
                what = CMD_WIFI_WAN_UP;
                break;
            case Zing5GRouterService.CELLULAR_WAN_TYPE:
                what = CMD_CELLULAR_WAN_UP;
                break;
            case Zing5GRouterService.ETHERNET_WAN_TYPE:
                what = CMD_ETHERNET_WAN_UP;
                break;
            default:
                log("wanInterfaceUp invalid WAN transport: " + transport);
                return;
        }
        mIfaceNetId.put(iface, netId);
        sendMessage(what, iface);
    }

    public void wanInterfaceDown(int transport, String iface) {
        int what;
        switch(transport) {
            case Zing5GRouterService.WIFI_WAN_TYPE:
                what = CMD_WIFI_WAN_DOWN;
                break;
            case Zing5GRouterService.CELLULAR_WAN_TYPE:
                what = CMD_CELLULAR_WAN_DOWN;
                break;
            case Zing5GRouterService.ETHERNET_WAN_TYPE:
                what = CMD_ETHERNET_WAN_DOWN;
                break;
            default:
                log("wanInterfaceUp invalid WAN transport: " + transport);
                return;
        }

        sendMessage(what, iface);
    }

    private boolean addUsbLanDefaultRoute(String iface) {
        if (!iface.isEmpty() && mIfaceNetId.containsKey(iface)) {
            log("Setting WAN " + iface + " as default route for USB LAN");
            mZRS.sendMessage(Zing5GRouterService.MODIFY_DEFAULT_ROUTE_TO_USB_LAN,
                    mIfaceNetId.get(iface),
                    Zing5GRouterService.ADD_DEFAULT_ROUTE, null);
            return true;
        }
        return false;
    }

    private boolean removeUsbLanDefaultRoute(String iface) {
        if (!iface.isEmpty() && mIfaceNetId.containsKey(iface)) {
            log("Removing WAN " + iface + " as default route for USB LAN");
            mZRS.sendMessage(Zing5GRouterService.MODIFY_DEFAULT_ROUTE_TO_USB_LAN,
                    mIfaceNetId.get(iface),
                    Zing5GRouterService.REMOVE_DEFAULT_ROUTE, null);
            return true;
        }
        return false;
    }


    private class WaitForLan extends State {
        private static final String STATE_TAG = "WaitForLan";
        @Override
        public void enter() {
            log(STATE_TAG + ".enter");
        }

        @Override
        public boolean processMessage(Message message) {
            log(STATE_TAG + ".processMessage what:" + getMessageName(message.what));

            switch(message.what) {
                case CMD_LAN_DETECTED:
                    mLanIface = (String) message.obj;
                    log("LAN iface:" + mLanIface);
                    transitionTo(mLanUp);
                    break;
                default:
                    log(STATE_TAG + " deferring unhandled message:" + getMessageName(message.what));
                    deferMessage(message);
                    break;
            }

            return HANDLED;
        }

        @Override
        public void exit() {
            log(STATE_TAG + ".exit");
        }
    }

    private class LanUp extends State {
        private static final String STATE_TAG = "LanUp";
        @Override
        public void enter() {
            log(STATE_TAG + ".enter");
        }

        @Override
        public boolean processMessage(Message message) {
            String wanIface = (String) message.obj;
            log(STATE_TAG + ".processMessage what: " + getMessageName(message.what)
                    + " for " + wanIface);

            switch (message.what) {
                case CMD_WIFI_WAN_UP:
                    mWifiWan = wanIface;
                    transitionTo(mWanW);
                    break;
                case CMD_CELLULAR_WAN_UP:
                    mCellularWan = wanIface;
                    transitionTo(mWanC);
                    break;
                case CMD_ETHERNET_WAN_UP:
                    mEthernetWan = wanIface;
                    transitionTo(mWanE);
                    break;
                case CMD_CELLULAR_WAN_DOWN:
                    mCellularWan = "";
                    break;
                case CMD_ETHERNET_WAN_DOWN:
                    mEthernetWan = "";
                    break;
                case CMD_WIFI_WAN_DOWN:
                    mWifiWan = "";
                    break;
                default:
                    log(STATE_TAG + " deferring unhandled message:" + getMessageName(message.what));
                    deferMessage(message);
                    break;
            }

            return HANDLED;
        }

        @Override
        public void exit() {
            log(STATE_TAG + ".exit");
        }
    }

    private class WAN_C extends State {
        private static final String STATE_TAG = "WAN_C";
        @Override
        public void enter() {
            log(STATE_TAG + ".enter");

            // Add cellular WAN as default gateway to USB LAN routing table
            if (addUsbLanDefaultRoute(mCellularWan) == false) {
                log(STATE_TAG + " ERROR WAN empty or netId unavailable!");
            }
        }

        @Override
        public boolean processMessage(Message message) {
            String wanIface = (String) message.obj;
            log(STATE_TAG + ".processMessage what: " +
                    getMessageName(message.what) + " for " + wanIface);

            switch (message.what) {
                case CMD_CELLULAR_WAN_UP:
                    // should not receive this event here!
                    break;
                case CMD_CELLULAR_WAN_DOWN:
                    // Linux kernel removes cellular default route from USB LAN routing table
                    mCellularWan = "";
                    transitionTo(mLanUp);
                    break;

                case CMD_WIFI_WAN_UP:
                    mWifiWan = wanIface;
                    removeUsbLanDefaultRoute(mCellularWan);
                    transitionTo(mWanCW);
                    break;
                case CMD_WIFI_WAN_DOWN:
                    mWifiWan = "";
                    break;

                case CMD_ETHERNET_WAN_UP:
                    mEthernetWan = wanIface;
                    removeUsbLanDefaultRoute(mCellularWan);
                    transitionTo(mWanCE);
                    break;
                case CMD_ETHERNET_WAN_DOWN:
                    mEthernetWan = "";
                    break;

                default:
                    log(STATE_TAG + " deferring unhandled message:" + getMessageName(message.what));
                    deferMessage(message);
                    break;
            }

            return HANDLED;
        }

        @Override
        public void exit() {
            log(STATE_TAG + ".exit");
        }
    }

    private class WAN_W extends State {
        private static final String STATE_TAG = "WAN_W";
        @Override
        public void enter() {
            log(STATE_TAG + ".enter");

            // Add WiFi WAN as default gateway to USB LAN routing table
            if (addUsbLanDefaultRoute(mWifiWan) == false) {
                log(STATE_TAG + " ERROR WAN empty or netId unavailable!");
            }
        }

        @Override
        public boolean processMessage(Message message) {
            String wanIface = (String) message.obj;
            log(STATE_TAG + ".processMessage what:" + getMessageName(message.what)
                    + " for " + wanIface);

            switch (message.what) {
                case CMD_WIFI_WAN_UP:
                    // Shouldn't get this event!
                    break;
                case CMD_WIFI_WAN_DOWN:
                    // Linux kernel removes WiFi default route from USB LAN routing table
                    mWifiWan = "";
                    transitionTo(mLanUp);
                    break;

                case CMD_CELLULAR_WAN_UP:
                    mCellularWan = wanIface;
                    removeUsbLanDefaultRoute(mWifiWan);
                    transitionTo(mWanCW);
                    break;
                case CMD_CELLULAR_WAN_DOWN:
                    mCellularWan = "";
                    break;

                case CMD_ETHERNET_WAN_UP:
                    mEthernetWan = wanIface;
                    removeUsbLanDefaultRoute(mWifiWan);
                    transitionTo(mWanWE);
                    break;
                case CMD_ETHERNET_WAN_DOWN:
                    mEthernetWan = "";
                    break;

                default:
                    log(STATE_TAG + " deferring unhandled message:" + getMessageName(message.what));
                    deferMessage(message);
                    break;
            }

            return HANDLED;
        }

        @Override
        public void exit() {
            log(STATE_TAG + ".exit");
        }
    }

    private class WAN_E extends State {
        private static final String STATE_TAG = "WAN_E";
        @Override
        public void enter() {
            log(STATE_TAG + ".enter");

            // Add cellular WAN as default gateway to USB LAN routing table
            if (addUsbLanDefaultRoute(mEthernetWan) == false) {
                log(STATE_TAG + " ERROR WAN empty or netId unavailable!");
            }
        }

        @Override
        public boolean processMessage(Message message) {
            String wanIface = (String) message.obj;
            log(STATE_TAG + ".processMessage what:" + getMessageName(message.what)
                    + " for " + wanIface);

            switch (message.what) {
                case CMD_ETHERNET_WAN_UP:
                    // Shouldn't get this event!
                    break;
                case CMD_ETHERNET_WAN_DOWN:
                    // Linux kernel removes Ethernet default route from USB LAN routing table
                    mEthernetWan = "";
                    transitionTo(mLanUp);
                    break;

                case CMD_WIFI_WAN_UP:
                    mWifiWan = wanIface;
                    removeUsbLanDefaultRoute(mEthernetWan);
                    transitionTo(mWanWE);
                    break;
                case CMD_WIFI_WAN_DOWN:
                    mWifiWan = "";
                    break;

                case CMD_CELLULAR_WAN_UP:
                    mCellularWan = wanIface;
                    removeUsbLanDefaultRoute(mEthernetWan);
                    transitionTo(mWanCE);
                    break;
                case CMD_CELLULAR_WAN_DOWN:
                    mCellularWan = "";
                    break;

                default:
                    log(STATE_TAG + " deferring unhandled message:" + getMessageName(message.what));
                    deferMessage(message);
                    break;
            }

            return HANDLED;
        }

        @Override
        public void exit() {
            log(STATE_TAG + ".exit");
        }
    }

    private class WAN_CW extends State {
        private static final String STATE_TAG = "WAN_CW";
        private String defaultIface = new String();
        @Override
        public void enter() {
            if (mCellularPriority >= mWifiPriority) {
                defaultIface = mCellularWan;
            } else {
                defaultIface = mWifiWan;
            }

            log(STATE_TAG + ".enter default iface: " + defaultIface);

            addUsbLanDefaultRoute(defaultIface);
        }

        @Override
        public boolean processMessage(Message message) {
            String wanIface = (String) message.obj;
            log(STATE_TAG + ".processMessage what:" + getMessageName(message.what)
                    + " for " + wanIface);

            switch (message.what) {
                case CMD_WIFI_WAN_UP:
                case CMD_CELLULAR_WAN_UP:
                    // Shouldn't get this event!
                    break;

                case CMD_WIFI_WAN_DOWN:
                    if (!defaultIface.equals(mWifiWan)) {
                        // To cover future cases when default interface for CW is not WiFi.
                        removeUsbLanDefaultRoute(defaultIface);
                    }
                    // The 'else' case, when defaultIface == mWifiWan is covered by Linux kernel.
                    // Linux kernel removes WiFi default route from USB LAN route table
                    // when WiFi interface goes down.
                    mWifiWan = "";
                    // When transitioning to mWanC there is no default route configured for USB LAN
                    transitionTo(mWanC);
                    break;

                case CMD_CELLULAR_WAN_DOWN:
                    if (!defaultIface.equals(mCellularWan)) {
                        removeUsbLanDefaultRoute(defaultIface);
                    }
                    mCellularWan = "";
                    transitionTo(mWanW);

                case CMD_ETHERNET_WAN_UP:
                    mEthernetWan = wanIface;
                    removeUsbLanDefaultRoute(defaultIface);
                    transitionTo(mWanWCE);
                    break;
                case CMD_ETHERNET_WAN_DOWN:
                    mEthernetWan = "";
                    break;

                default:
                    log(STATE_TAG + " deferring unhandled message:" + getMessageName(message.what));
                    deferMessage(message);
                    break;
            }

            return HANDLED;
        }

        @Override
        public void exit() {
            log(STATE_TAG + ".exit");
        }
    }

    private class WAN_WE extends State {
        private static final String STATE_TAG = "WAN_WE";
        private String defaultIface = new String();
        @Override
        public void enter() {
            if (mEthernetPriority >= mWifiPriority) {
                defaultIface = mEthernetWan;
            } else {
                defaultIface = mWifiWan;
            }

            log(STATE_TAG + ".enter default iface: " + defaultIface);

            addUsbLanDefaultRoute(defaultIface);
        }

        @Override
        public boolean processMessage(Message message) {
            String wanIface = (String) message.obj;
            log(STATE_TAG + ".processMessage what:" + getMessageName(message.what)
                    + " for " + wanIface);

            switch (message.what) {
                case CMD_WIFI_WAN_UP:
                case CMD_ETHERNET_WAN_UP:
                    // Shouldn't get this event!
                    break;

                case CMD_WIFI_WAN_DOWN:
                    if (!defaultIface.equals(mWifiWan)) {
                        // To cover future cases when default interface for WE is not Ethernet.
                        removeUsbLanDefaultRoute(defaultIface);
                    }
                    // The 'else' case, when defaultIface == mEthernetWan is covered by Linux kernel.
                    // Linux kernel removes Ethernet default route from USB LAN route table
                    // when Ethernet interface goes down.
                    mWifiWan = "";
                    // When transitioning to mWanE there is no default route configured for USB LAN
                    transitionTo(mWanE);
                    break;

                case CMD_ETHERNET_WAN_DOWN:
                    if (!defaultIface.equals(mEthernetWan)) {
                        removeUsbLanDefaultRoute(defaultIface);
                    }
                    mEthernetWan = "";
                    transitionTo(mWanW);
                    break;

                case CMD_CELLULAR_WAN_UP:
                    mCellularWan = wanIface;
                    removeUsbLanDefaultRoute(defaultIface);
                    transitionTo(mWanWCE);
                    break;
                case CMD_CELLULAR_WAN_DOWN:
                    mCellularWan = "";
                    break;

                default:
                    log(STATE_TAG + " deferring unhandled message:" + getMessageName(message.what));
                    deferMessage(message);
                    break;
            }

            return HANDLED;
        }

        @Override
        public void exit() {
            log(STATE_TAG + ".exit");
        }
    }

    private class WAN_CE extends State {
        private static final String STATE_TAG = "WAN_CE";
        private String defaultIface = new String();
        @Override
        public void enter() {
            if (mCellularPriority >= mEthernetPriority) {
                defaultIface = mCellularWan;
            } else {
                defaultIface = mEthernetWan;
            }

            log(STATE_TAG + ".enter default iface: " + defaultIface);

            addUsbLanDefaultRoute(defaultIface);
        }

        @Override
        public boolean processMessage(Message message) {
            String wanIface = (String) message.obj;
            log(STATE_TAG + ".processMessage what:" + getMessageName(message.what)
                    + " for " + wanIface);

            switch (message.what) {
                case CMD_ETHERNET_WAN_UP:
                case CMD_CELLULAR_WAN_UP:
                    // Shouldn't get this event!
                    break;

                case CMD_ETHERNET_WAN_DOWN:
                    if (!defaultIface.equals(mEthernetWan)) {
                        removeUsbLanDefaultRoute(defaultIface);
                    }
                    mEthernetWan = "";
                    transitionTo(mWanC);
                    break;

                case CMD_CELLULAR_WAN_DOWN:
                    if (!defaultIface.equals(mCellularWan)) {
                        removeUsbLanDefaultRoute(defaultIface);
                    }
                    mCellularWan = "";
                    transitionTo(mWanE);
                    break;

                case CMD_WIFI_WAN_UP:
                    mWifiWan = wanIface;
                    removeUsbLanDefaultRoute(defaultIface);
                    transitionTo(mWanWCE);
                    break;
                case CMD_WIFI_WAN_DOWN:
                    mWifiWan = "";
                    break;

                default:
                    log(STATE_TAG + " deferring unhandled message:" + getMessageName(message.what));
                    deferMessage(message);
                    break;
            }

            return HANDLED;
        }

        @Override
        public void exit() {
            log(STATE_TAG + ".exit");
        }
    }

    private class WAN_WCE extends State {
        private static final String STATE_TAG = "WAN_WCE";
        private String defaultIface = new String();
        @Override
        public void enter() {
            if (mCellularPriority >= mEthernetPriority && mCellularPriority >= mWifiPriority) {
                defaultIface = mCellularWan;
            } else {  // Cellular WAN does not have the highest priority
                if (mEthernetPriority >= mWifiPriority) {
                    defaultIface = mEthernetWan;
                } else {
                    defaultIface = mWifiWan;
                }
            }

            log(STATE_TAG + ".enter default iface: " + defaultIface);

            addUsbLanDefaultRoute(defaultIface);
        }

        @Override
        public boolean processMessage(Message message) {
            String wanIface = (String) message.obj;
            log(STATE_TAG + ".processMessage what:" + getMessageName(message.what)
                    + " for " + wanIface);

            switch (message.what) {
                case CMD_ETHERNET_WAN_UP:
                case CMD_CELLULAR_WAN_UP:
                case CMD_WIFI_WAN_UP:
                    // Shouldn't get this event!
                    break;

                case CMD_ETHERNET_WAN_DOWN:
                    if (!defaultIface.equals(mEthernetWan)) {
                        removeUsbLanDefaultRoute(defaultIface);
                    }
                    mEthernetWan = "";
                    transitionTo(mWanCW);
                    break;

                case CMD_CELLULAR_WAN_DOWN:
                    if (!defaultIface.equals(mCellularWan)) {
                        removeUsbLanDefaultRoute(defaultIface);
                    }
                    mCellularWan = "";
                    transitionTo(mWanWE);
                    break;

                case CMD_WIFI_WAN_DOWN:
                    if (!defaultIface.equals(mWifiWan)) {
                        removeUsbLanDefaultRoute(defaultIface);
                    }
                    mWifiWan = "";
                    removeUsbLanDefaultRoute(defaultIface);
                    transitionTo(mWanCE);
                    break;

                default:
                    log(STATE_TAG + " deferring unhandled message:" + getMessageName(message.what));
                    deferMessage(message);
                    break;
            }

            return HANDLED;
        }

        @Override
        public void exit() {
            log(STATE_TAG + ".exit");
        }
    }

    private final State mWaitForLan = new WaitForLan();
    private final State mLanUp = new LanUp();
    private final State mWanC = new WAN_C();
    private final State mWanW = new WAN_W();
    private final State mWanE = new WAN_E();
    private final State mWanCW = new WAN_CW();
    private final State mWanWE = new WAN_WE();
    private final State mWanCE = new WAN_CE();
    private final State mWanWCE = new WAN_WCE();
}