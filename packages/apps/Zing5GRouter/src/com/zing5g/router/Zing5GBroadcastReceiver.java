package com.zing5g.router;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Zing5GBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = Zing5GBroadcastReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.i(TAG, "Received BOOT_COMPLETED");

            // Start Zing5GRouterService
            Zing5GRouterService z5gRouter = Zing5GRouterService.getInstance(context.getApplicationContext());
            z5gRouter.sendMessage(Zing5GRouterService.ZING_5G_ROUTER_BOOTED);
        }
    }

    // Call this method to fetch AP configuration. 
    // Currently method sends the config to logcat.
    private void JobIntentServiceBootCompleted(Context context) {
        Intent jobIntent = new Intent(context, Zing5GJobIntentService.class);
        jobIntent.putExtra(Zing5GJobIntentService.GET_AP_CONFIG, true);
        Zing5GJobIntentService.enqueueWork(context, jobIntent);
    }
}
