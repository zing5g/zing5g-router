package com.zing.webserver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.util.*;
import java.io.*;
import java.util.Map;
import android.net.wifi.WifiConfiguration;

import static android.os.Environment.getExternalStorageState;
import static java.lang.String.*;
import com.android.server.connectivity.*;

import com.zing.webserver.ZingJobIntentService;
import com.zing.xmlparser.ZingXmlConfig;
import com.zing.xmlparser.xmlSerializer;

public class AndroidWebServer extends NanoHTTPD {

    String TAG = "ZingWebServer";
    private static AndroidWebServer webserver_instance = null;
    private boolean ap_changed = false ;
    private static Context mContext = null;

    //messages to betn intent service and receiver. Eventually a string table can be used
    public final  static String ZING_BOOT_COMPLETED= "Boot Completed";
    public final  static String ZING_SOFTAP_STATE_CHANGED= "SoftAP State Changed";
    public final  static String ZING_SOFTAP_CONFIG_CHANGED= "AP configruration Changed";
    public final  static String ZING_WEBPAGE_MAIN= "/Main page";
    public final  static String ZING_WEBPAGE_DHCP= "/Dhcp Settings";


    private String cur_page = ZING_WEBPAGE_MAIN;
    static private boolean webserverRunning = false;
    private HashMap<String, String> mHashConfig =  null ;

    private AndroidWebServer(int port) {
        super(port);
    }
    static boolean isWebserverRunning(){
        return webserverRunning;
    }
    public void startWebServer() {
        Log.d(TAG,"Before Start Android Webserver >>>>>>>>>webserverRunning >>>>>>"+AndroidWebServer.webserverRunning);
        if (AndroidWebServer.webserverRunning == true) return;
        try {
            mHashConfig = ZingXmlConfig.getInstance().getZRouterConfig();
            start(0);/*  time out - 0 -wait forever*/
            AndroidWebServer.webserverRunning = true;
            Log.d(TAG,"Start Android Webserver >>>>>>>>>>>>>>>");
        }
        catch (IOException ex) {
            AndroidWebServer.webserverRunning = false;
            Log.e(TAG, "Error to start webserver ",ex);
        }
    }
    public void stopWebServer(){
        stop();
        webserverRunning = false;
    }
    // static method to create instance of Singleton class
    public static AndroidWebServer getInstance(Context context) {
        if (webserver_instance == null) {
            mContext = context;
            webserver_instance = new com.zing.webserver.AndroidWebServer(8080);
        }

        return webserver_instance;
    }


    public AndroidWebServer(String hostname, int port) {
        super(hostname, port);
        Log.d(TAG,"Start Android Webserver >>>>>>>>>>>>>>> this object "+ this.toString());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public Response serve(IHTTPSession session) {
        Method method = session.getMethod();
        String uri = session.getUri();

        Log.d(TAG, "In Serve-  Method Name: "+ method.name()+" method Name"+method.toString()+"URI : "+uri );
        Map<String, String> parms = session.getParms();
        //ZingNwInfo zingInfo = ZingNwInfo.getInstance();

        switch (method.toString()) {
            case "GET":
                if (uri.equals("/favicon.ico"))   //Seems Virus, not sure ? Seems some special case to be handled
                    return newFixedLengthResponse(null);
                else if (uri.equals(ZING_WEBPAGE_DHCP)) {
                    cur_page = ZING_WEBPAGE_DHCP;
                    break;
                } else if (uri.equals(ZING_WEBPAGE_MAIN)) {
                    cur_page = ZING_WEBPAGE_MAIN;
                    break;
                }

                Log.d(TAG, "BEFORE SWITCH URI in Method GET cur_page = "+cur_page );
                switch (uri) {

                    case "/HOST_BASE_ADDR":
                        String str= parms.get("Host IP Adress");
                        if(validIP(str)== true)
                        {
                            mHashConfig.put("HOST_BASE_ADDRESS", str );
                            Log.d(TAG, "adding HOST BASE ADres in mHashConfig"+str);
                        }
                        Log.d(TAG, "CASE GET ------User input IP ADDRESS " +str + " output hashmap " + mHashConfig.get("HOST_BASE_ADDRESS"));
                        break;
                    case "/DNS1":
                        str= parms.get("DNS Server");
                        if(validIP(str) == true)  mHashConfig.put("DHCP_SERVER_ADDRESS", str );
                        Log.d(TAG, "CASE GET ------User input DNS1  " + str + " output hashmap " + mHashConfig.get("DHCP_SERVER_ADDRESS"));
                        break;
                    case "/PREFIX_LEN":
                        str= parms.get("Prefix Length");
                        if(validaNumber(str)== true)  mHashConfig.put("PREFIX_LEN", str );
                        Log.d(TAG, "CASE GET ---------------User input Prefix Length " + str + " output hashmap " + mHashConfig.get("PREFIX_LEN"));
                        break;

                    case "/LEASE_PERIOD":
                        str= parms.get("Lease Period");
                        if(validaNumber(str)== true)  mHashConfig.put("LEASE_PERIOD", str );
                        Log.d(TAG, "CASE GET ---------------User input Prefix Length " + str + " output hashmap " + mHashConfig.get("LEASE_PERIOD"));
                        break;
                    case "/SERVER_ADDR":
                        str= parms.get("DHCP Server Address");
                        if(validaNumber(str)== true)  mHashConfig.put("DHCP_SERVER_ADDRESS", str );
                        Log.d(TAG, "CASE GET ---------------User input Prefix Length " + str + " output hashmap " + mHashConfig.get("DHCP_SERVER_ADDRESS"));
                        break;
                    case "/SSID_NAME":
                        str= parms.get("SSID name");
                        if ( str != null && !str.isEmpty())
                        {
                            mHashConfig.put("SSID_NAME", str );
                            ap_changed = true;
                            Log.d(TAG, "CASE GET ---------------User input SSID NAme" + str + " output hashmap " + mHashConfig.get("SSID_NAME"));
                        }
                        break;
                    case "/SHARED_KEY":
                        str= parms.get("Password");
                        if ((str != null)&&(!str.isEmpty()))
                        {
                            mHashConfig.put("SHARED_KEY", str );
                            ap_changed = true;
                        }
                        Log.d(TAG, "CASE GET ---------------User input SHARED_KEY " + str + " output hashmap " + mHashConfig.get("SSID_NAME"));
                        break;

                    default:
                        mHashConfig = ZingXmlConfig.getInstance().getZRouterConfig();
                        Log.d(TAG, "CASE GET ---------------DEFAULT URI ----HOST_BASE_ADDR-------");

                        break;
                }
                break;
            case "POST":
                Log.d(TAG, "IN SWITCH ---------------CASE POST ------------- URI : "+uri);
                switch (uri) {
                    case "/UpdateConfFile":
                        Log.d(TAG, "CASE POST ---------------UPDATE CONFIG FILE -- "+ mHashConfig.get("HOST_BASE_ADDRESS"));

                        (new xmlSerializer()).generatePublicXml(mHashConfig);

                        //send
                        if (ap_changed == true) {

                            Log.d(TAG, "CASE POST ---------------UPDATE  AP name and key-- "+ mHashConfig.get("SSID_NAME") + " "+ mHashConfig.get("SHARED_KEY"));
                            Log.d(TAG, "CASE POST ---------------WebServer context-- "+mContext.toString());
                            //Intent jobIntent = new Intent(null, ZingJobIntentService.class);
                            //Intent jobIntent = new Intent(mContext, ZingJobIntentService.class );
                            //jobIntent.setAction(com.zing.webserver.AndroidWebServer.ZING_SOFTAP_CONFIG_CHANGED);
                            //ZingJobIntentService.enqueueWork(mContext, jobIntent);

                            WifiManager mWifiManager =null;
                            if ((mContext== null) || (mWifiManager = (WifiManager) mContext.getSystemService(mContext.WIFI_SERVICE))==null)

                            {
                                Log.d("ZingAP", "  NULL context or wifimanager, StartSofAPMode something wrong, return ******************** ");
                               break;
                            }

                            WifiConfiguration mApConfiguration = mWifiManager.getWifiApConfiguration();
                            if  (ZingXmlConfig.mhashMapconfigs == null )   {
                                ZingXmlConfig.mhashMapconfigs  = ZingXmlConfig.getInstance().getZRouterConfig();
                            }
                            mApConfiguration.SSID = ZingXmlConfig.mhashMapconfigs.get("SSID_NAME");
                            mApConfiguration.preSharedKey = ZingXmlConfig.mhashMapconfigs.get("SHARED_KEY");


                            mWifiManager.setWifiApConfiguration(mApConfiguration);
                            Log.d(TAG, "  Check IsWifiApenabled >>>>>>>>>>>>>>>>>>>>>>>>>>>> "+mWifiManager.isWifiApEnabled());
                            if (mWifiManager.isWifiApEnabled() == true) {
                                Log.d("ZingAP", "  Wifi AP IS ON, start again >>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
                                mWifiManager.stopSoftAp();
                                try {
                                    Thread.sleep(500);
                                }catch (InterruptedException ex) {
                                    Log.d("ZingAP", "  Wifi AP IS ON, Thread exception <<<<<<<<<<< ");
                                }
                                mWifiManager.startSoftAp(mApConfiguration);

                            }

                            ap_changed = false ;
                        }

                        break;
                    case "/UpdateNetwork": //NOT Getting here , This place is to directly updating network link
                        Log.d(TAG, "CASE POST ---------------UPDATE NEtwork  ----------------");
                        break;
                    default:break;
                }

            default:
                break;
        }

        String answer = buildHTML(cur_page);

        Log.d(TAG,answer);
        return newFixedLengthResponse(answer);

    }

    boolean validaNumber(String str)
    {
        boolean ret = true;
        if (str == null || str.isEmpty()) return false;
        try {
            Integer.parseInt(str);
        }catch (NumberFormatException e){
          ret = false;
        }
        return ret;
    }
    private boolean validIP(String ip) {

        try {
            if ( ip == null || ip.isEmpty() ) {
                return false;
            }

            String[] parts = ip.split( "\\." );
            if ( parts.length != 4 ) {
                return false;
            }

            for ( String s : parts ) {
                int i = Integer.parseInt( s );
                if ( (i < 0) || (i > 255) ) {
                    return false;
                }
            }
            if ( ip.endsWith(".") ) {
                return false;
            }
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return false;
        }
        Log.d(TAG, " >>>>>>>>>>>>>>>>>. valid IP true >>>>>>>>>>>>>");
        return true;
    }



    @RequiresApi(api = Build.VERSION_CODES.O)
    private String buildHTML(String page)
    {
        int n=24;
        List<String> respList = new Vector<String>(n);

        AddTitleHeader(respList);

        if (page.equals(ZING_WEBPAGE_MAIN) ) {
            insertStringLabel_Get(respList,"SSID_NAME", "SSID name",  mHashConfig.get("SSID_NAME"));
            insertStringLabel_Get(respList,"SHARED_KEY", "Password",  mHashConfig.get("SHARED_KEY"));
            respList.add("<a href=\"Dhcp Settings\" style=\"text-align:right;\" <h2>ZING5G ROUTER DHCP Server Settings _ LINK </h2> </a>");
        }
        else if (page.equals(ZING_WEBPAGE_DHCP) ) {
            insertStringLabel_Get(respList,"HOST_BASE_ADDR", "Host IP Adress", mHashConfig.get("HOST_BASE_ADDRESS"));
            insertStringLabel_Get(respList,"PREFIX_LEN", "Prefix Length", mHashConfig.get("PREFIX_LEN"));
            insertStringLabel_Get(respList,"DNS1", "DNS Server",  mHashConfig.get("DHCP_SERVER_ADDRESS"));
            insertStringLabel_Get(respList,"LEASE_PERIOD", "Lease Period",  mHashConfig.get("LEASE_PERIOD"));
            insertStringLabel_Get(respList,"SERVER_ADDR", "DHCP Server Address",  mHashConfig.get("DHCP_SERVER_ADDRESS"));
            respList.add("<a href=\"Main page\" style=\"text-align:right;\" <h2>ZING5G Main AP Settings _ LINK </h2> </a>");
        }

       // ZingNwInfo zingInfo = ZingNwInfo.getInstance();


        endHeaderHTML(respList);

        String listString = "";

        for (String s : respList)
        {
            listString += s + "\t";
        }

        return listString;

    }


    private void    AddTitleHeader(List<String> resp)
    {
        String header = "<!DOCTYPE html><html><body><h1>ZING5G ROUTER </h1>\n" +
                "<h2>Quectel EVB </h2>";

        resp.add(header);
    }


    private  void endHeaderHTML(List<String> resp)
    {
        String header = "<form action='UpdateConfFile?' method='post'><h3> <button type=\"submit\">UPLOAD TO CONF FILE!</button></h3> </form> </body></html>";
        resp.add(header);
    }

    private void insertStringLabel_Get(List<String> resp, String URI, String label, String inpVal)
    {
        String output = String.format("<form action='%s?' method='get'> <p>%2$s : <input type='text' name='%s' value=\"%s\"></p></form> ",URI,label,inpVal, label);
        resp.add(output);
    }

}

