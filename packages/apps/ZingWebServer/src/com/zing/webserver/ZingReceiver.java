package com.zing.webserver;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import android.content.BroadcastReceiver;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;

public class ZingReceiver extends BroadcastReceiver {
    Intent mServiceIntent;
   // private ZingService mZingService;
    Context ctx;
    String TAG = "ZingWebSever";
    public Context getCtx() {
        return ctx;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.d(TAG, " Webserver recevied - Broadcase Receiver Boot_COMPLETED >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

            Intent jobIntent = new Intent(context, ZingJobIntentService.class);
            jobIntent.setAction(AndroidWebServer.ZING_BOOT_COMPLETED);
            ZingJobIntentService.enqueueWork(context, jobIntent);
        }
    }
}
