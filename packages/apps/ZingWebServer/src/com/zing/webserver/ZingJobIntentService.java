package com.zing.webserver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;
import com.android.server.connectivity.*;///////////

import com.zing.xmlparser.ZingXmlConfig;
import com.zing.xmlparser.xmlSerializer;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

public class ZingJobIntentService extends JobIntentService {
    private AndroidWebServer androidWebServer;
    protected static WifiManager mWifiManager;
	protected WifiConfiguration mApConfiguration;
    private static Context mContext = null;
    private final String TAG = "ZingWebServer" ;
    static private boolean bWebserverStart = false ;

    public static void enqueueWork(Context context, Intent intent) {
        if (context != null){
            mContext=context;
        }
 	Log.d("ZingWebServer", "ZING INTENT SERVICE ==========enqueueWork"+ mContext.toString() + " intent" + intent.getAction() );
	enqueueWork(context, ZingJobIntentService.class, 1111, intent); //1111 not used
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "============= OnHandleWork  AndroidWebServer - FROM ZING INTENT SERVICE ==============action===================="+ action);
        switch (intent.getAction() ){
            case AndroidWebServer.ZING_BOOT_COMPLETED:
               // Context context = getApplicationContext();
                Log.d(TAG, "============= OnHandleWork  AndroidWebServer case AndroidWebServer.ZING_BOOT_COMPLETED bWebserverStart="+ZingJobIntentService.bWebserverStart);
                if (ZingJobIntentService.bWebserverStart == false ) {
                    startAndroidWebServer(mContext);
                }
                break;
            case AndroidWebServer.ZING_SOFTAP_CONFIG_CHANGED:
                Context context = getApplicationContext();
                Log.d(TAG, "============= OnHandleWork  AndroidWebServer ====ZING_SOFTAP_CONFIG_CHANGED=============context = "+ context.toString());
                WifiManager mWifiManager =null;
                if ((context== null) || (mWifiManager = (WifiManager) context.getSystemService(context.WIFI_SERVICE))==null)

                {
                    Log.d("ZingAP", "  NULL context or wifimanager, StartSofAPMode something wrong, return ******************** ");
                    return;
                }

                WifiConfiguration mApConfiguration = mWifiManager.getWifiApConfiguration();
                if  (ZingXmlConfig.mhashMapconfigs == null )   {
                    ZingXmlConfig.mhashMapconfigs  = ZingXmlConfig.getInstance().getZRouterConfig();
                }
                mApConfiguration.SSID = ZingXmlConfig.mhashMapconfigs.get("SSID_NAME");
                mApConfiguration.preSharedKey = ZingXmlConfig.mhashMapconfigs.get("SHARED_KEY");


                mWifiManager.setWifiApConfiguration(mApConfiguration);
            default: break;
        }
    }
        
    private void startAndroidWebServer(Context context) {
        try {
 	        Log.d(TAG, "============= TO start  AndroidWebServer -  ==================================");
            androidWebServer = AndroidWebServer.getInstance(context);
            ZingJobIntentService.bWebserverStart = true ;
            androidWebServer.startWebServer();

           
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return;
    }

    private boolean stopAndroidWebServer() {
        if (androidWebServer != null) {
            androidWebServer.stopWebServer();
            ZingJobIntentService.bWebserverStart = false ;
            return true;
        }
        return false;
    }

}

