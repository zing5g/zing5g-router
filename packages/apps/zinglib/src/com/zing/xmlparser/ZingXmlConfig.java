package com.zing.xmlparser;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.io.File;

public class ZingXmlConfig {
    private static String routerConfigFileName = "/data/ZRouterConf.xml";
    private String TAG = ZingXmlConfig.class.getSimpleName();
    public static HashMap<String, String> mhashMapconfigs = null ;
    private static ZingXmlConfig mInstance = null;

    public static boolean configFileExists() {
        File configFile = new File(routerConfigFileName);
        return configFile.exists();
    }

    public static ZingXmlConfig getInstance(){
        if (mInstance== null)
            mInstance = new ZingXmlConfig();
        return mInstance;
    }
    public HashMap<String, String> getZRouterConfig(){
        if (configFileExists()) {
            parse();
        } else {
            Log.i(TAG, "getZRouterConfig no config file exists");
        }
        return mhashMapconfigs;
    }
    public String getIPAddress(){
        return mhashMapconfigs.get("HOST_BASE_ADDRESS");
    }

    public String setIPAddress(String addr){
        return mhashMapconfigs.put("HOST_BASE_ADDRESS", addr);
    }

    private void parse(){

        XmlPullParserFactory factory = null;//
        int eventType = 0;
        String tagname = null;
        String tagval = null;
        int noOfRecords = 0;
	FileInputStream fis = null;
	InputStreamReader input = null ;
        mhashMapconfigs = new HashMap<String, String>();

        try {
            factory = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        factory.setNamespaceAware(true);
        XmlPullParser xpp = null;
        try {
            xpp = factory.newPullParser();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        try {
            fis = new FileInputStream(routerConfigFileName);
	    input = new InputStreamReader(fis);
            xpp.setInput(input);

        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }

        try {
            eventType = xpp.getEventType();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String name = null;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                    break;
                case  XmlPullParser.START_TAG:
                    tagname =xpp.getName();
                    Log.d(TAG,"Start tag "+tagname);
                    break;
                case XmlPullParser.TEXT:
                    tagval = xpp.getText();
                    Log.d(TAG,"Start TEXT "+tagval);
                    break;
                case  XmlPullParser.END_TAG:
                    if(xpp.getName().equals(tagname)) {
                        mhashMapconfigs.put(tagname, tagval);noOfRecords++;
                        Log.d(TAG,"END TAG tag and value "+tagname +tagval);
                    }
                    break;
                default: break;
            }
            try {            
                eventType = xpp.next();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
        }
        
        Log.d(TAG,"Hashmap  Entries -- size--- "+ + mhashMapconfigs.size() +  mhashMapconfigs );
	try {            
            	fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
  	 try {            
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        
	///////////
    }
}
