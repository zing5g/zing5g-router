package com.zing.xmlparser;

import android.provider.ContactsContract;
import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.HashMap;


import android.util.Log;

public class xmlSerializer {
    String TAG = "Zing" ;

    public void generatePublicXml(HashMap<String, String> config)  {
        XmlSerializer serial = Xml.newSerializer();

        File fileConf = null;
        try {
            fileConf = new File("/data/ZRouterConf.xml");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        /*BufferedWriter fWriter = null;
        try {
            fWriter = new BufferedWriter(new FileWriter(fileConf));
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        Log.d(TAG," -->>>>>>>NOn NULL ?????????? >fileConf .tpo path" + fileConf.getPath());
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(fileConf);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.d(TAG," -->>>>>>>NOn NULL ?????????? >>outstream .tostring" + outStream.toString());
        try {
            serial.setOutput(outStream, null);
            Log.d(TAG,"serial.tostring  "+serial.toString());
            serial.startDocument(null, null);
            for (String name : config.keySet()) {
                serial.startTag(null, name);
     		   serial.text(config.get(name));     
		//serial.attribute(null, "type", config.get(name));
                serial.endTag(null, name );
            }
            serial.endDocument();
            serial.flush();
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG,"Hashmap  Entries-- after entering --- "+  config + " no of records " + config.size() );
    }
}
