LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_MODULE := zinglib

LOCAL_MODULE_TAGS := optional

LOCAL_PACKAGE_NAME := zinglib

include $(BUILD_JAVA_LIBRARY)


