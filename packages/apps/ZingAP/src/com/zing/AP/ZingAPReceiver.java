package com.zing.ap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ZingAPReceiver extends BroadcastReceiver {
    private static final String TAG = ZingAPReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.i(TAG, "Received BOOT_COMPLETED");

            Intent jobIntent = new Intent(context, ZingAPJobIntentService.class);
            jobIntent.setAction(Zing5gApplication.ZING_BOOT_COMPLETED);
            ZingAPJobIntentService.enqueueWork(context, jobIntent);
        }
    }
}
