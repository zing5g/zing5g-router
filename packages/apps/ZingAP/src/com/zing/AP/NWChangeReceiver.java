package com.zing.ap;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import java.util.concurrent.Executor;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

//Following receiver events NETWORK_STATE_CHANGED_ACTION, registered in Zing5gApplication onCreate, and writes new NW status into file in separarte thread  
public class  NWChangeReceiver extends BroadcastReceiver {
	String TAG = "ZingAP" ; 
	
	
	@Override
	public void onReceive(Context context, Intent intent) 
	{
				
		// Only Write Current AP state into Config file, Other logs are for debug/support puprose only 
    		
		NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
    		
		String stat= info.getDetailedState().toString();
            	Log.i(TAG, "in Class NWChangeReceiver - "+info.getDetailedState().toString());

            	if (stat.equals("CONNECTED") ||stat.equals("DISCONNECTED")   ) {
			Intent jobIntent = new Intent(context, ZingAPJobIntentService.class);
			jobIntent.setAction(Zing5gApplication.ZING_WIFI_NW_CHANGE);
			ZingAPJobIntentService.enqueueWork(context, jobIntent);
            		//	executr.execute(thread1);
		}
            	else	
                	Log.i(TAG, "Ignored");

    		
	}

}
