package com.zing.ap;

import android.annotation.Nullable;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network; 
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.SoftApCallback;
import android.util.Log;

import android.content.Context;
import android.content.Intent;

import android.net.Network;
import java.net.InetAddress;
import java.net.UnknownHostException;

import java.util.concurrent.Executor;

import  java.util.concurrent.locks.*;

import android.os.RemoteException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;

import java.util.*;
//
import com.zing.xmlparser.ZingXmlConfig;
import com.zing.xmlparser.xmlSerializer;
import java.util.HashMap;



public class WifiApManager {

	private WifiManager mWifiManager;
	
	public static WifiApManager mApmgr_instance = null ; 

	WifiConfiguration mApConfiguration;

	private static String TAG = "ZingAP";

	public static WifiApManager getInstance() {
		if (mApmgr_instance == null)
			mApmgr_instance = new WifiApManager();

			return mApmgr_instance;
	}

	public void CheckConfig_StartAPmodeOnBoot(){
		String prvStatus = "TRUE";
		boolean configFile = ZingXmlConfig.configFileExists();
		Log.i(TAG, "CheckConfig_StartAPmodeOnBoot configFile: " + configFile);

		if (configFile) {
			if  (ZingXmlConfig.mhashMapconfigs == null )   {
				ZingXmlConfig.mhashMapconfigs  = ZingXmlConfig.getInstance().getZRouterConfig();
			}
			if (ZingXmlConfig.mhashMapconfigs == null ||
				ZingXmlConfig.mhashMapconfigs.size() == 0 ){
				Log.i(TAG,"NO CONFIG FILE OR TAGS MISSING  <<<<<<<<<<<<<<<<<<<<,, ");
			}
			prvStatus = ZingXmlConfig.mhashMapconfigs.get("AP_STORED_STATE");
		}
		startAPmode(prvStatus.equals("TRUE"));
		Zing5gApplication.setBootStatus(true);
		return ; 
	}

	public void stopAPmode() { // This is not called, just funciton kept incase 
		Log.d(TAG,"STOP AP Mode <<<<<<<<<<<<<<<<<<<<,, ");
		//if ((mConnectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE)) != null)
		//	mConnectivityManager.stopTethering(0);
		//else
		//	Log.d(TAG,"Null Connectivity Manager in STOP Tehering, unabel to stop <<<<<<<<<<<<<<<<<<<<,, ");	
    	}

	private void startAPmode( boolean bstart){
		Log.i(TAG,"startAPmode =========== "+ bstart);
		startSoftAPMode(bstart);
	}

	private WifiManager.SoftApCallback mSoftApCallback = new WifiManager.SoftApCallback() {

		@Override
		public void onStateChanged(int state, int failureReason) {
			Log.d("ZingAP", " SoftApCallback onStateChanged state =******************** "+ state);
			Intent jobIntent = new Intent(Zing5gApplication.getContext(), ZingAPJobIntentService.class);						
			jobIntent.setAction(Zing5gApplication.ZING_SOFTAP_STATE_CHANGED);
			ZingAPJobIntentService.enqueueWork(Zing5gApplication.getContext(), jobIntent);
		}

		@Override
		public void onNumClientsChanged(int numClients) {
			// Do nothing - we don't care about changing anything here.
		}

		@Override
		public void onStaConnected(String Macaddr, int numClients) {
			Log.d("ZingAP", " SoftApCallback onStaConnected ******************** ");
		}

		@Override
		public void onStaDisconnected(String Macaddr, int numClients) {
			Log.d("ZingAP", " SoftApCallback onStaDisconnected ******************** ");
		}
	};
  
  	private void startSoftAPMode( boolean bstart){
		boolean configFile = ZingXmlConfig.configFileExists();
		Log.i(TAG, "startSoftAPMode configFile: " + configFile);

		Context context = Zing5gApplication.getContext();
		if ((mWifiManager = (WifiManager) context.getSystemService(context.WIFI_SERVICE))==null) {
			Log.d("ZingAP", "  NULL wifimanager, StartSofAPMode something wrong, return ******************** ");
			return;
		}
		mWifiManager.registerSoftApCallback(mSoftApCallback, null);	
	
		mApConfiguration = mWifiManager.getWifiApConfiguration();
		if (configFile) {
			if (ZingXmlConfig.mhashMapconfigs == null) {
				ZingXmlConfig.mhashMapconfigs = ZingXmlConfig.getInstance().getZRouterConfig();
			}
			mApConfiguration.SSID = ZingXmlConfig.mhashMapconfigs.get("SSID_NAME");
			mApConfiguration.preSharedKey = ZingXmlConfig.mhashMapconfigs.get("SHARED_KEY");
		} else {
			Log.i(TAG, "startSoftAPMode using default AP configuration");
		}

		mWifiManager.setWifiApConfiguration(mApConfiguration); 
		if (bstart){
			mWifiManager.startSoftAp(mApConfiguration);  
	 		Log.d("ZingAP", "  startSoftAp called , Tethering  start >>>>>>>>>>>>>>>> ");
		}
		else
			Log.d("ZingAP", " startSoftAp NOT called , May be prev state not enabled <<<<<<<<<<<<<<<< ");
		
	}
}
