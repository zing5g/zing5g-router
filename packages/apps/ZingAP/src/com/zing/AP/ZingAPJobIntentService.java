package com.zing.ap;


import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;
////
import android.net.ConnectivityManager;


////
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;


import com.zing.xmlparser.ZingXmlConfig;
import com.zing.xmlparser.xmlSerializer;
import java.util.HashMap;

public class ZingAPJobIntentService extends JobIntentService {
    private final String TAG = ZingXmlConfig.class.getSimpleName();;
    protected static WifiManager mWifiManager;
    protected WifiConfiguration mApConfiguration;
    private boolean configFileExists = false;
   
    public static void enqueueWork(Context context, Intent intent) {

        enqueueWork(context, ZingAPJobIntentService.class, 23, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
	 	String action = intent.getAction();
        switch (intent.getAction() ){
           
            case Zing5gApplication.ZING_BOOT_COMPLETED:
                //  following to start router in AP mode on BOOT event
                Context context = (Zing5gApplication.getContext().getApplicationContext());
                mWifiManager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
                if (mWifiManager == null){
                    Log.i(TAG, "mWifiManager NULL, returning with no action on BOOT COMPLETED ???????????? ");
                    return;
                }

                mApConfiguration = mWifiManager.getWifiApConfiguration();
                Log.i(TAG,  "BOOT_COMPLETED ssid " + mApConfiguration.SSID+", key "+mApConfiguration.preSharedKey);
                Log.i(TAG, mApConfiguration.toString());

                WifiApManager apmode =  WifiApManager.getInstance();
                configFileExists = ZingXmlConfig.configFileExists();
                Log.i(TAG, "Config file exists: " + configFileExists);
                apmode.CheckConfig_StartAPmodeOnBoot();
                break;

            case Zing5gApplication.ZING_SOFTAP_STATE_CHANGED:
            case Zing5gApplication.ZING_WIFI_NW_CHANGE:
                    Log.i(TAG, "Zing Jobintent service  intentAction = >>>>>>>>>>"+intent.getAction() );

                    if (configFileExists == false)
                        break;
                    if (Zing5gApplication.getBootStatus()== false)  break;
                    if (ZingXmlConfig.mhashMapconfigs == null) {
                        ZingXmlConfig.mhashMapconfigs = ZingXmlConfig.getInstance().getZRouterConfig();
                    }
                    //ApIOConf.confReadWriteAPMode(false); // write into file
                    int curTetherState = mWifiManager.getWifiApState() ;
                    mApConfiguration = mWifiManager.getWifiApConfiguration();

                    if (curTetherState == 13)
                        ZingXmlConfig.mhashMapconfigs.put("AP_STORED_STATE", "TRUE");
                    else
                        ZingXmlConfig.mhashMapconfigs.put("AP_STORED_STATE", "FALSE");

                    ZingXmlConfig.mhashMapconfigs.put("SSID_NAME", mApConfiguration.SSID );
                    ZingXmlConfig.mhashMapconfigs.put("SHARED_KEY", mApConfiguration.preSharedKey);

                    new xmlSerializer().generatePublicXml(ZingXmlConfig.mhashMapconfigs);

                    Log.d(TAG, "Hashmap  Entries --after reading - no of records " + ZingXmlConfig.mhashMapconfigs.size());

                    break;
            default:
                break;

        }

       return;
	
     }
}
