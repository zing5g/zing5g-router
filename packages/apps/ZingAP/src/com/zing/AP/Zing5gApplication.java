package com.zing.ap;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

public class Zing5gApplication extends Application {
//Application to hold main configurations, application context and to register network change events. 
//However TETHERING call backs are done in WifiAPManager class which can be moved to the app. 

	private static Context mContext= null;
    	private static String mconfFileName="APMode1.conf";
    	public static WifiApManager mWifiApmgr = null;

	String TAG = "ZingAP" ;
	private static boolean BOOT_COMPLETE_STATUS = false; 
    

	public final  static String ZING_WIFI_NW_CHANGE= "NW WIFI STATE Change";
	public final  static String ZING_BOOT_COMPLETED= "Boot Completed";
	public final  static String ZING_SOFTAP_STATE_CHANGED= "SoftAP State Changed";

	
	static public boolean getBootStatus(){
		return BOOT_COMPLETE_STATUS ;	
	}
 	static public void setBootStatus(boolean set){
		BOOT_COMPLETE_STATUS = set ;
		return;	
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = getApplicationContext();
	   	Log.i(TAG, "ON ROUTER APPLICATION ON CREATE ++++++++++  ");
		
		registerNWStateChangeAction();

    	}
	
	public void registerNWStateChangeAction(){
		NWChangeReceiver receiver =  new NWChangeReceiver();		
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
		registerReceiver(receiver, intentFilter);
	
	}



	public static Context getContext() {
		return mContext;
	}
    
    	public static String getConfFileName() {
   		return mconfFileName; 
    	} 
}
